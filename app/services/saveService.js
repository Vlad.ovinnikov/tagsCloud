(function(){
   var SaveService = function(){
       var matchedTexts = [];
       return {
           add: function(datasource, article) {
               angular.forEach(datasource, function(art, key){
                   angular.forEach(art.tags, function (tag, key) {
                       if (article.name == tag) {
                           matchedTexts.push(art);
                        }
                    })
                });
               return matchedTexts;
           },
           remove: function(datasource, article) {
               angular.forEach(datasource, function(art, key){
                   angular.forEach(art.tags, function (tag, key) {
                       if (article.name == tag) {
                           matchedTexts.splice(art, 1);
                        }
                    })
                });
               return matchedTexts;
           }
       }
   } 
   angular.module('tagscloud').factory('SaveService', SaveService);
})();